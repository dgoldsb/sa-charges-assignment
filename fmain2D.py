# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 11:51:58 2015

@author: Dylan

In this module, we do something.
"""

import numpy as np
import math
import matplotlib.pyplot as plt

CONST_RAD = 1
  
class System(object):
    def __init__(self, chargecount):
        self.allCharges = np.array([[0,0,0]])
        self.chargecount = chargecount
        for i in range(0,chargecount):
            self.add_charge(i)
        self.allCharges = self.allCharges[1:]
        
    def add_charge(self, iden):
        a = np.random.uniform(0,2*math.pi)
        r = np.random.uniform(0,CONST_RAD)
        xcoor = r * math.cos(a)
        ycoor = r * math.sin(a)
        self.allCharges = np.append(self.allCharges,
            [[iden,xcoor,ycoor]],axis=0)
    
    def total_energy(self, allArr):
        total_energy = 0
        for i in range(0,self.chargecount):
            for j in range(0,self.chargecount):
                if i != j:    
                    chargeA = allArr[i]
                    chargeB = allArr[j]
                    xdif = math.fabs(chargeA[1]-chargeB[1])
                    ydif = math.fabs(chargeA[2]-chargeB[2])
                    distance = math.sqrt(xdif*xdif + ydif*ydif)
                    total_energy = total_energy + (1/distance)                    
        return total_energy

    def forcevector(self,allArr,chargeA):
        "Do for the closest charge to A"
        maxforce = 0
        maxi = 0
        for i in range(0,self.chargecount):
            chargeB = allArr[i]            
            if chargeA[0]!=chargeB[0]:  
                xdif = math.fabs(chargeA[1]-chargeB[1])
                ydif = math.fabs(chargeA[2]-chargeB[2])
                distance = math.sqrt(xdif*xdif + ydif*ydif)
                force = 1/(distance**3)
                if force > maxforce:
                    maxforce = force
                    maxi = i
        chargeB = allArr[maxi]
        a = (chargeA[1]-chargeB[1])
        b = (chargeA[2]-chargeB[2])
        angle = math.atan(b/a)
        maxran = 1 - 1/maxforce
        size = np.random.uniform(0,maxran)    
        return [angle,size]
        
    def random_movement(self,allArr,maxStep,heurProb):
        index = np.random.uniform(0,len(allArr))      
        charge = allArr[index]
        """80 percent of the time we use the closest force"""        
        if np.random.uniform(0,1) > (1-heurProb):
            forcevec = self.forcevector(allArr,charge)
            a_new = forcevec[0]
            r_new = forcevec[1]*maxStep
        else:
            a_new = np.random.uniform(0,2*math.pi)
            r_new = np.random.uniform(0,maxStep)        
        xcoor = charge[1]
        ycoor = charge[2]
        newx = xcoor + r_new * math.cos(a_new)
        newy = ycoor + r_new * math.sin(a_new)
        if  (math.sqrt(newx*newx + newy*newy) > 1):
            """Put on boundary, use sine rule"""         
            r_old = math.sqrt(xcoor**2+ycoor**2)            
            a_old = math.atan(ycoor/xcoor)
            C = math.asin(r_old*math.sin(a_old+a_new))
            A = (math.pi-C)/2
            newx = math.cos(a_old-A)
            newy  = math.sin(a_old-A)   
        charge[1] = newx
        charge[2] = newy
        allArr[index] = charge
        return allArr
            
    def acceptance_probability(self, old, new, temp):
        return np.exp(-math.fabs(old-new)/temp)
    
    def hillclimb(self,allArr):
        """Difference when it converges"""
        convergingDif = 0.00001
        """Amount of iterations before it checks to stop"""
        runIterations = 10000     
        
        state = np.copy(allArr)
        min_energy = self.total_energy(state)
        prev_energy = 2*self.total_energy(state)
        min_state = np.copy(state)
        ts_energy = np.array([])
        maxStep = 1
        
        j = 0
        while ((prev_energy-min_energy)/prev_energy) > convergingDif:
            print((prev_energy-min_energy)/prev_energy) 
            stepVar = ((prev_energy-min_energy)/prev_energy)    
            prev_energy = min_energy
            i=0
            while i < runIterations:
                state = np.copy(min_state)
                maxStep = 2*stepVar
                state = self.random_movement(state,maxStep,0)
                energy = self.total_energy(state)
                ts_energy = np.append(ts_energy, min_energy)
                if energy < min_energy:
                    min_energy = energy
                    min_state = np.copy(state)
                i += 1
            j += 1
            maxStep = maxStep
        """Alternative: set original starting state back"""
        """Plot or return the min_state"""
        self.plotSystem(min_state,1)
        self.plotTSh(ts_energy)
        return min_state
    
    def anneal(self, allArr):
        """Exponential temperature parameters"""        
        T_start = 10000
        coeff = -5
        T_min = math.pow(10,coeff)
        T_best = T_start
        alpha = 0.9

        """Linear temperature parameters"""
        beta = 2000
        coeff2 = -5
        T_min2 = math.pow(10,coeff2)

        repeats = 50*100
        
        state  = np.copy(allArr)
        energy = self.total_energy(state)
        min_energy = energy
        min_state = state
        ts_energy = np.array([])
        ts_T = np.array([])
        maxStep = 1
        
        T = T_start
        energy = min_energy
        z = 1
        while T > T_min:
            for k in range(0,repeats):  
                old_state = np.copy(state)
                maxStep = 0.005+0.995*(T/T_start)                   
                hprob = 0
                if T < (T_start/2):
                    hprob = 0
                new_state = self.random_movement(state,maxStep,hprob)
                new_energy = self.total_energy(state)
                if energy < min_energy:
                    min_energy = energy
                    min_state = np.copy(new_state)
                    T_best = T
                ap = self.acceptance_probability(energy,new_energy,T)
                if new_energy < energy or ap > np.random.rand():
                    state = np.copy(new_state)
                    energy = new_energy
                else:
                    state = np.copy(old_state)
                ts_energy = np.append(ts_energy, energy)
                ts_T = np.append(ts_T, T)
            
#           Exponential multiplicative cooling
            T = T*alpha

#           Linear multiplicative cooling
#           T = T_start/(1+beta*z)
            z = z+1


        """Difference when it converges"""
        convergingDif = 0.0001
        """Amount of iterations before it checks to stop"""
        runIterations = 10000 

        state = np.copy(min_state)
        min_energy = self.total_energy(state)
        prev_energy = 2*self.total_energy(state)
        maxStep = 1
        
        j = 0
        while ((prev_energy-min_energy)/prev_energy) > convergingDif:
            print((prev_energy-min_energy)/prev_energy) 
            stepVar = ((prev_energy-min_energy)/prev_energy)    
            prev_energy = min_energy
            i=0
            while i < runIterations:
                state = np.copy(min_state)
                maxStep = 2*stepVar
                state = self.random_movement(state,maxStep,0)
                energy = self.total_energy(state)
                ts_energy = np.append(ts_energy, min_energy)
                ts_T = np.append(ts_T, T)
                if energy < min_energy:
                    min_energy = energy
                    min_state = np.copy(state)
                i += 1
            j += 1
            maxStep = maxStep

        """ Plot the final state """
        self.plotSystem(min_state,2)
        self.plotTS(ts_energy,ts_T)
        return min_state
    
    def plotSystem(self,allArr,k):
        x = np.zeros(len(allArr))
        y = np.zeros(len(allArr))
        for i in range(0,len(allArr)):
            x[i] = allArr[i][1]
            y[i] = allArr[i][2]
        circle = plt.Circle((0,0),CONST_RAD,color='w',ec='k',zorder=0)
        circle1 = plt.Circle((0,0),CONST_RAD/2,color='w',ec='k',zorder=2)
        circle2 = plt.Circle((0,0),CONST_RAD/4,color='w',ec='k',zorder=3)
        circle3 = plt.Circle((0,0),
            (3*CONST_RAD)/4,color='w',ec='k',zorder=1)
        fig = plt.figure(k)
        ax = plt.gca()
        ax.scatter(x,y,zorder=5)
        fig.gca().add_artist(circle)
        fig.gca().add_artist(circle1)
        fig.gca().add_artist(circle2)
        fig.gca().add_artist(circle3)
        plt.xlim(-1,1)
        plt.ylim(-1,1)
        plt.axes().set_aspect('equal')
        plt.title('Locations of charges in space')
        plt.xlabel('x')
        plt.ylabel('y')
        if k%2 == 0:
            plt.savefig('2d_a_system.pdf', format='pdf')
        else:
            plt.savefig('2d_h_system.pdf', format='pdf')
    
    def plotTS(self,ts_energy,ts_T):
        plt.figure(4,facecolor="white")
    
        plt.subplot(211) 
        plt.title('Waiting time timeseries')
        plt.xlabel('Iteration')
        plt.ylabel('System energy')
        plt.ylim(0,(3)*np.median(ts_energy))
        plt.plot(ts_energy,'k')
        plt.subplot(212) 
        plt.title('Temperature')
        plt.xlabel('Iteration')
        plt.ylabel('Temperature')
        plt.yscale('log')
        plt.ylim(np.min(ts_T)/10,(1)*np.max(ts_T))
        plt.plot(ts_T, 'r--')
    
        plt.tight_layout()
        plt.savefig('2d_a_energy.pdf', format='pdf')

    def plotTSh(self,ts_energy):

        plt.figure(3)
        plt.plot(ts_energy)
        plt.ylim(0,2*np.median(ts_energy))
        plt.title('Total energy over time')
        plt.xlabel('Iteration')
        plt.ylabel('Total energy')
        plt.savefig('2d_h_energy.pdf', format='pdf')

    
if __name__ == "__main__":
    system = System(50)
    print('Starting energy of the system: %.2f' 
        %system.total_energy(system.allCharges))
    endstate=system.hillclimb(system.allCharges)    
    print('Ending energy of the system: %.2f'%system.total_energy(endstate))
    endstate=system.anneal(system.allCharges)    
    print('Ending energy of the system: %.2f' 
        %system.total_energy(endstate))
    
"""
References:
    http://math.stackexchange.com/questions/1365622/adding-two-polar-vectors
"""