# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 11:51:58 2015

@author: Dylan

In this module, we do something.
"""

import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

CONST_RAD = 1
  
class System(object):
    def __init__(self, chargecount):
        self.allCharges = np.array([[0,0,0,0]])
        self.chargecount = chargecount
        for i in range(0,chargecount):
            self.add_charge(i)
        self.allCharges = self.allCharges[1:]
        
    def add_charge(self, iden):
        r = np.random.uniform(0,CONST_RAD)
        phi = np.random.uniform(0,2*math.pi)
        theta = np.random.uniform(0,2*math.pi)
        xcoor = r * math.cos(phi) * math.sin(theta)
        ycoor = r * math.sin(theta) * math.sin(phi)
        zcoor = r * math.cos(theta)
        self.allCharges = np.append(self.allCharges,
            [[iden,xcoor,ycoor,zcoor]],axis=0)
    
    def total_energy(self, allArr):
        total_energy = 0
        for i in range(0,self.chargecount):
            for j in range(0,self.chargecount):
                if i != j:    
                    chargeA = allArr[i]
                    chargeB = allArr[j]
                    xdif = math.fabs(chargeA[1]-chargeB[1])
                    ydif = math.fabs(chargeA[2]-chargeB[2])
                    zdif = math.fabs(chargeA[3]-chargeB[3])
                    distance = math.sqrt(xdif*xdif + ydif*ydif + zdif*zdif)
                    total_energy = total_energy + (1/distance)                    
        return total_energy
    
    def forcevector(self,allArr,chargeA):
        "Do for the closest charge to A"
        maxforce = 0
        maxi = 0
        for i in range(0,self.chargecount):
            chargeB = allArr[i]            
            if chargeA[0]!=chargeB[0]:  
                xdif = math.fabs(chargeA[1]-chargeB[1])
                ydif = math.fabs(chargeA[2]-chargeB[2])
                zdif = math.fabs(chargeA[3]-chargeB[3])
                distance = math.sqrt(xdif*xdif + ydif*ydif + zdif*zdif)
                force = 1/(distance**3)
                if force > maxforce:
                    maxforce = force
                    maxi = i
        chargeB = allArr[maxi]
        a = (chargeA[1]-chargeB[1])
        b = (chargeA[2]-chargeB[2])
        c = (chargeA[2]-chargeB[2])
        r = math.sqrt(a*a + b*b + c*c)
        phi = math.atan(b/a)
        theta = math.acos(c/r)
        maxran = 1 - 1/maxforce
        size = np.random.uniform(0,maxran)    
        return [theta,phi,size]
        
    def random_movement(self,allArr,maxStep,heurprob):
        index = np.random.uniform(0,len(allArr))      
        charge = allArr[index]

#       80 percent of the time we use the closest force      
        if np.random.uniform(0,1) > (1-heurprob):
            forcevec = self.forcevector(allArr,charge)
            theta = forcevec[0]
            phi = forcevec[1]
            r_new = forcevec[2]*maxStep
        else:
            theta = np.random.uniform(0,2*math.pi)
            phi = np.random.uniform(0,2*math.pi)
            r_new = np.random.uniform(0,maxStep)        
        a = charge[1]
        b = charge[2]
        c = charge[3]

        newx = a + r_new * math.cos(phi) * math.sin(theta)
        newy = b + r_new * math.sin(theta) * math.sin(phi)
        newz = c + r_new * math.cos(theta)
        if  ((newx*newx + newy*newy + newz*newz) > 1):
#           I asked the Mathematica god to find for which r_new we intersect
#           the sphere, fullsimplify did not simplify a whole lot :'(

            r_new = ((-2*c*math.cos(theta)
                -2*a*math.cos(phi)*math.sin(theta)
                -2*b*math.sin(phi)*math.sin(theta)
                -math.sqrt(math.pow(2*c*math.cos(theta)
                    +2*a*math.cos(phi)*math.sin(theta)
                    +2*b*math.sin(phi)*math.sin(theta),2)
                -4*(-1 + math.pow(a,2) + math.pow(b,2) + math.pow(c,2))
                *(math.pow(math.cos(theta),2) 
                    +math.pow(math.cos(phi),2)*math.pow(math.sin(theta),2)
                    +math.pow(math.sin(phi),2)* math.pow(math.sin(theta),2))))
            /(2*(math.pow(math.cos(theta),2)
                +math.pow(math.cos(phi),2)*math.pow(math.sin(theta),2)
                +math.pow(math.sin(phi),2)*math.pow(math.sin(theta),2))))

            newx = a + r_new * math.cos(phi) * math.sin(theta)
            newy = b + r_new * math.sin(theta) * math.sin(phi)
            newz = c + r_new * math.cos(theta)

        charge[1] = newx
        charge[2] = newy
        charge[3] = newz
        allArr[index] = charge
        return allArr
            
    def acceptance_probability(self, old, new, temp):
        return np.exp(-math.fabs(old-new)/temp)
    
    def hillclimb(self,allArr):
        """Difference when it converges"""
        convergingDif = 0.0001
        """Amount of iterations before it checks to stop"""
        runIterations = 10000     
        
        state = np.copy(allArr)
        min_energy = self.total_energy(state)
        prev_energy = 2*self.total_energy(state)
        min_state = np.copy(state)
        ts_energy = np.array([])
        maxStep = 1
        
        j = 0
        while ((prev_energy-min_energy)/prev_energy) > convergingDif:
            print((prev_energy-min_energy)/prev_energy) 
            stepVar = ((prev_energy-min_energy)/prev_energy)    
            prev_energy = min_energy
            i=0
            while i < runIterations:
                state = np.copy(min_state)
                maxStep = 2*stepVar
                state = self.random_movement(state,maxStep,0)
                energy = self.total_energy(state)
                ts_energy = np.append(ts_energy, min_energy)
                if energy < min_energy:
                    min_energy = energy
                    min_state = np.copy(state)
                i += 1
            j += 1
            maxStep = maxStep
        """Alternative: set original starting state back"""
        """Plot or return the min_state"""
        self.plotSystem(min_state,1)
        self.plotTSh(ts_energy)
        return min_state
    
    def anneal(self, allArr):
        """Exponential temperature parameters"""        
        T_start = 10000
        coeff = -7
        T_min = math.pow(10,coeff)
        T_best = T_start
        alpha = 0.9

        """Linear temperature parameters"""
        beta = 10000
        coeff2 = -1
        T_min2 = math.pow(10,coeff2)

        """Mini-hillclimbs"""
        repeats = 25*100
        hcits = 0

        state  = np.copy(allArr)
        energy = self.total_energy(state)
        min_energy = energy
        min_state = state
        ts_energy = np.array([])
        ts_T = np.array([])
        maxStep = 1
        
        T = T_start
        energy = min_energy
        z = 1
        while T > T_min:
            maxStep = 0.005+0.995*(T/T_start) 
            lowest_loop_state = np.copy(state) 
            for k in range(0,repeats):  
                old_state = np.copy(state)         
                hprob = 0
                if T > (T_start/2):
                    hprob = 0.0 
                new_state = self.random_movement(state,maxStep,hprob)
                new_energy = self.total_energy(state)
                if energy < min_energy:
                    min_energy = energy
                    min_state = np.copy(new_state)
                ap = self.acceptance_probability(energy,new_energy,T)
                if new_energy < energy or ap > np.random.rand():
                    state = np.copy(new_state)
                    energy = new_energy
                else:
                    state = np.copy(old_state)
                ts_energy = np.append(ts_energy, energy)
                ts_T = np.append(ts_T, T)
#           Exponential multiplicative cooling
            T = T*alpha

#           Linear multiplicative cooling
#           T = T_start/(1+beta*z)
            z = z+1

        """Difference when it converges"""
        convergingDif = 0.0001
        """Amount of iterations before it checks to stop"""
        runIterations = 10000 

        state = np.copy(min_state)
        min_energy = self.total_energy(state)
        prev_energy = 2*self.total_energy(state)
        maxStep = 1
        
        j = 0
        while ((prev_energy-min_energy)/prev_energy) > convergingDif:
            print((prev_energy-min_energy)/prev_energy) 
            stepVar = ((prev_energy-min_energy)/prev_energy)    
            prev_energy = min_energy
            i=0
            while i < runIterations:
                state = np.copy(min_state)
                maxStep = 2*stepVar
                state = self.random_movement(state,maxStep,0)
                energy = self.total_energy(state)
                ts_energy = np.append(ts_energy, min_energy)
                ts_T = np.append(ts_T, T)
                if energy < min_energy:
                    min_energy = energy
                    min_state = np.copy(state)
                i += 1
            j += 1
            maxStep = maxStep

        self.plotSystem(min_state,2)
        self.plotTS(ts_energy,ts_T)
        return min_state
    
    def plotSystem(self,allArr,k):
        x = np.zeros(len(allArr))
        y = np.zeros(len(allArr))
        z = np.zeros(len(allArr))
        for i in range(0,len(allArr)):
            x[i] = allArr[i][1]
            y[i] = allArr[i][2]
            z[i] = allArr[i][3]

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(x, y, z, c='r', marker='o')

        u = np.linspace(0, 2 * np.pi, 100)
        v = np.linspace(0, np.pi, 100)

        xs = 1 * np.outer(np.cos(u), np.sin(v))
        ys = 1 * np.outer(np.sin(u), np.sin(v))
        zs = 1 * np.outer(np.ones(np.size(u)), np.cos(v))
        ax.plot_surface(xs, ys, zs, rstride=4, cstride=4, color='b', alpha=0.1)

        plt.title('Locations of charges in space')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        if k%2 == 0:
            plt.savefig('3d_a_system.pdf', format='pdf')
        else:
            plt.savefig('3d_h_system.pdf', format='pdf')
        plt.show()
    
    def plotTS(self,ts_energy,ts_T):
        plt.figure(4,facecolor="white")
    
        plt.subplot(211) 
        plt.title('Waiting time timeseries')
        plt.xlabel('Iteration')
        plt.ylabel('System energy')
        plt.ylim((0),(3)*np.median(ts_energy))
        plt.plot(ts_energy,'k')
        plt.subplot(212) 
        plt.title('Temperature')
        plt.xlabel('Iteration')
        plt.ylabel('Temperature')
        plt.ylim(np.min(ts_T)/10,(1)*np.max(ts_T))
        plt.yscale('log')
        plt.plot(ts_T, 'r--')
        plt.tight_layout()
        plt.savefig('3d_a_energy.pdf', format='pdf')
        plt.show()

    def plotTSh(self,ts_energy):

        plt.figure(3)
        plt.plot(ts_energy)
        plt.ylim(((np.min(ts_energy))-10),2*np.median(ts_energy))
        plt.title('Total energy over time')
        plt.xlabel('Iteration')
        plt.ylabel('Total energy')
        plt.savefig('3d_h_energy.pdf', format='pdf')
        plt.show()

    
if __name__ == "__main__":
    system = System(25)
#   http://www.mathpages.com/home/kmath005/kmath005.htm
    print('Starting energy of the system: %.2f' 
        %system.total_energy(system.allCharges))
    endstate=system.hillclimb(system.allCharges)    
    print('Ending energy of the system: %.2f'%system.total_energy(endstate))
    endstate=system.anneal(system.allCharges)    
    print('Ending energy of the system: %.2f' 
        %system.total_energy(endstate))

"""
References:
    http://math.stackexchange.com/questions/1365622/adding-two-polar-vectors
"""


"""
#               Do  some hillclimbing steps to quickly explore this state
                hc_state = np.copy(state)
                hc_energy = energy
                for m in range(0,hcits):
                    old_hc_state = np.copy(hc_state)             
                    new_hc_state = self.random_movement(hc_state,maxStep,hprob)
                    new_hc_energy = self.total_energy(new_hc_state)
                    if energy < min_energy:
                        min_energy = energy
                        min_state = np.copy(new_hc_state)
                    if new_energy < energy:
                        hc_state = np.copy(new__hc_state)
                        hc_energy = new_hc_energy
                    else:
                        hc_state = np.copy(old_hc_state)
                    
                    ts_energy = np.append(ts_energy, hc_energy)
                    ts_T = np.append(ts_T, T)
#               Save the lowest overall
                if self.total_energy(lowest_loop_state)>hc_energy:
                    lowest_loop_state = np.copy(hc_state)
"""