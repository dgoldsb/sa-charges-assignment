## -*- coding: utf-8 -*-
#"""
#Created on Tue Dec  1 11:51:58 2015
#
#@author: Dylan
#
#In this module, we do something.
#"""
#
#import numpy as np
#import math
#import matplotlib.pyplot as plt
#
#CONST_RAD = 1
#
#class Charge(object):
#    def __init__(self, iden, xcoor, ycoor):
#        self.iden = iden
#        self.coordinates = np.array([xcoor,ycoor])
#    def __repr__(self):
#        return '{}@{}'.format(self.iden, self.coordinates)
#  
#class System(object):
#    def __init__(self, chargecount):
#        self.allCharges = np.array([]) 
#        for i in range(0,chargecount):
#            self.add_charge(i)
#        
#    def add_charge(self, iden):
#        a = np.random.uniform(0,2*math.pi)
#        r = np.random.uniform(0,CONST_RAD)
#        xcoor = r * math.cos(a)
#        ycoor = r * math.sin(a)
#        self.allCharges = np.append(self.allCharges,Charge(iden,xcoor,ycoor))
#    
#    def total_energy(self, allArr):
#        total_energy = 0
#        for chargeA in allArr:
#            for chargeB in allArr:
#                if chargeA != chargeB:             
#                    xdif = math.fabs(chargeA.coordinates[0]-chargeB.coordinates[0])
#                    ydif = math.fabs(chargeA.coordinates[1]-chargeB.coordinates[1])
#                    distance = math.sqrt(xdif*xdif + ydif*ydif)
#                    total_energy = total_energy + (1/distance)                    
#        if total_energy<1.01:
#            self.plotSystem(allArr)
#            print('start')
#            print(distance)
#            print(total_energy)
#        return total_energy
#    
#    def forcevector(self,allArr,chargeA):
#        "Do for the closest charge to A"
#        maxforce = 0
#        maxi = 0
#        for i in range(0,len(allArr)):
#            chargeB = allArr[i]            
#            if chargeA != chargeB:  
#                xdif = math.fabs(chargeA.coordinates[0]-chargeB.coordinates[0])
#                ydif = math.fabs(chargeA.coordinates[1]-chargeB.coordinates[1])
#                distance = math.sqrt(xdif*xdif + ydif*ydif)
#                force = 1/(distance**3)
#                if force > maxforce:
#                    maxforce = force
#                    maxi = i
#        chargeB = allArr[maxi]
#        a = (chargeA.coordinates[0]-chargeB.coordinates[0])
#        b = (chargeA.coordinates[1]-chargeB.coordinates[1])
#        angle = math.atan(b/a)
#        maxran = 1 - 1/maxforce
#        size = np.random.uniform(0,maxran)    
#        return [angle,size]
#        
#    def random_movement(self,allArr):
#        charge = allArr[np.random.uniform(0,len(allArr))]        
#        if np.random.uniform(0,1) > 0:
#            forcevec = self.forcevector(allArr,charge)
#            a_new = forcevec[0]
#            r_new = forcevec[1] 
#        else:
#            a_new = np.random.uniform(0,2*math.pi)
#            r_new = np.random.uniform(0,CONST_RAD)        
#        xcoor = charge.coordinates[0]
#        ycoor = charge.coordinates[1]
#        newx = xcoor + r_new * math.cos(a_new)
#        newy = ycoor + r_new * math.sin(a_new)
#        if  (math.sqrt(newx*newx + newy*newy) > 1):
#            """Put on boundary, use sine rule"""         
#            r_old = (xcoor**2+ycoor**2)            
#            a_old = math.atan(ycoor/xcoor)
#            C = math.asin(r_old*math.sin(a_old+a_new))
#            A = (math.pi-C)/2
#            newx = math.cos(a_old-A)
#            newy  = math.sin(a_old-A)   
#        charge.coordinates[0] = newx
#        charge.coordinates[1] = newy
#        return allArr
#            
#    def acceptance_probability(self, old, new, temp):
#        return np.exp((old-new)/temp)
#    
#    def hillclimb(self,allArr):
#        """Difference when it converges"""
#        convergingDif = 10**-5
#        """Amount of iterations before it checks to stop"""
#        runIterations = 1000        
#        
#        state = allArr
#        min_energy = self.total_energy(state)
#        prev_energy = 2*self.total_energy(state)
#        min_state = state
#        ts_energy = np.array([])
#        
#        j = 0
#        while ((prev_energy-min_energy)/prev_energy) > convergingDif:
#            print(j)           
#            prev_energy = min_energy
#            i=0
#            while i < runIterations:
#                state = min_state
#                state = self.random_movement(state)
#                energy = self.total_energy(state)
#                if energy < min_energy:
#                    min_energy = energy
#                    min_state = state
#                ts_energy = np.append(ts_energy, min_energy)
#                i += 1
#            j += 1
#        """Alternative: set original starting state back"""
#        """Plot or return the min_state"""
#        state = min_state
#        print(self.total_energy(min_state))
##        self.plotSystem(state)
#        self.plotTS(ts_energy)
#        return ts_energy
#    
#    def anneal(self, allArr):
#        """Temperature parameters"""        
#        T_start = 10000
#        T_min = 0.1
#        alpha = 0.9
#        """Number of reheats"""
#        restarts = 30
#        reheatEffect = 10
#        
#        state  = allArr
#        energy = self.total_energy(state)
#        min_energy = energy
#        min_state = state
#        ts_energy = np.array([])
#        
#        for iteration in range(restarts):
#            energy = min_energy
#            T = T_start
#            while T > T_min:
#                new_state = self.random_movement(state)
#                new_energy = self.total_energy(state)
#                if energy < min_energy:
#                    min_energy = energy
#                    min_state = new_state
#                ap = self.acceptance_probability(energy, new_energy, T)/((iteration+1)/reheatEffect)
#                if new_energy < energy or ap > np.random.rand():
#                    state = np.copy(new_state)
#                    energy = new_energy
#                ts_energy = np.append(ts_energy, energy)
#                T = T*alpha
#        """ Plot the final state """
#        self.plotSystem(min_state)
#        self.plotTS(ts_energy)
#        return ts_energy
#    
#    def plotSystem(self,allArr):
#        x = np.zeros(len(allArr))
#        y = np.zeros(len(allArr))
#        for i in range(0,len(allArr)):
#            x[i] = allArr[i].coordinates[0]
#            y[i] = allArr[i].coordinates[1]
#        circle = plt.Circle((0,0), CONST_RAD, color='w', ec='k', zorder=0)
#        fig = plt.figure(1)
#        ax = plt.gca()
#        ax.scatter(x,y)
#        fig.gca().add_artist(circle)
#        plt.xlim(-1,1)
#        plt.ylim(-1,1)
#        plt.show()
#    
#    def plotTS(self,ts_energy):
#        plt.figure(2)
#        plt.plot(ts_energy)
#        plt.show()
#    
#if __name__ == "__main__":
#    system = System(4)
#    print('Starting energy of the system: %.2f' %system.total_energy(system.allCharges))
#    system.anneal(system.allCharges)    
#    
#    
#"""
#References:
#    http://math.stackexchange.com/questions/1365622/adding-two-polar-vectors
#"""
