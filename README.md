## Synopsis

This project uses simulated annealing to find optimal energy configurations in 3D and 2D for Coulomb charges in a sphere or on a disc.

## Motivation

The project is the second assignement for the **Stochastic Simulation** course.

## Installation

The folder contains several .py files, the final versions are fmain2D.py for the 2D version, and fmain3D.py for the 3D version.

## Execution

Upon running first a greedy algorithm will be ran, then the simulated annealing implementation.

## Contributors

Author is Dylan Goldsborough (11066393), dylan.goldsborough@student.uva.nl at University of Amsterdam.